<?php

namespace CheeCodes\TelegramSdk\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class RegisterWebhookCommand extends Command
{
    protected $signature   = 'telegram:webhook {route=telegram-sdk.webhook}';

    protected $description = 'Register the webhook with telegram';

    public function handle() {
        $routeName = $this->argument('route');

        if ( !Route::has($routeName)) {
            $this->error(sprintf('Route "%s" does not exist', $routeName));

            return 1;
        }

        $url    = URL::route($routeName);
        $secret = config('services.telegram.token');

        Http::post('https://api.telegram.org/bot' . $secret . '/setWebhook', [
            'url' => $url,
        ]);

        $this->line(sprintf('Webhook was set to "%s"', $url));

        return 0;
    }
}
