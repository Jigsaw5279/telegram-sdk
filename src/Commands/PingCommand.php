<?php

namespace CheeCodes\TelegramSdk\Commands;

use CheeCodes\TelegramSdk\Telegram\InlineKeyboardMarkup;
use CheeCodes\TelegramSdk\Telegram\OutgoingMessage;
use CheeCodes\TelegramSdk\Telegram\Update;

class PingCommand extends BotCommandBase
{

    public function handle(Update $update, array $arguments, string $commandName, ?string $botName) {
        if ( !$update->isCallbackQuery()) {
            $message = OutgoingMessage::create(['text' => 'Pong: ' . implode(', ', $arguments)]);

            $count = $arguments[0] ?? '';

            if (is_numeric($count)) {
                $count = (int)$count;
                $message->setReplyMarkup(
                    InlineKeyboardMarkup::create()
                                        ->addButton([
                                            'text'          => 'Increase',
                                            'callback_data' => '/ping ' . $count + 1,
                                        ])
                                        ->nextRow()
                                        ->addButton([
                                            'text'          => 'Decrease',
                                            'callback_data' => '/ping ' . $count - 1,
                                        ])
                );
            }

            $update->getMessage()
                   ?->replyWithMessage($message);
        } else {
            $update->getCallbackQuery()?->answer([
                'text'       => 'Done: ' . $arguments[0],
                'show_alert' => true,
            ]);
        }
    }
}
