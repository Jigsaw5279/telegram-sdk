<?php

namespace CheeCodes\TelegramSdk\Commands;

use CheeCodes\TelegramSdk\Telegram\Update;

abstract class BotCommandBase
{
    abstract public function handle(Update $update, array $arguments, string $commandName, ?string $botName);
}
