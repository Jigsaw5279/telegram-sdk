<?php

namespace CheeCodes\TelegramSdk\Support;

use Illuminate\Support\Str;

class CommandParser
{
    private ?string $commandName  = null;

    private ?string $botName      = null;

    private array   $argumentList = [];

    public function parse(string $commandString) {
        preg_match_all('#.*/([a-z]+)(@[a-z]+)?\s?(.*)#is', $commandString, $matches);
        $this->commandName = $matches[1][0] ?? null;
        $this->assignBotName($matches[2][0] ?? null);
        $this->assignArgumentList($matches[3][0] ?? null);
    }

    public function getCommandName(): ?string {
        return $this->commandName;
    }

    public function getBotName(): ?string {
        return $this->botName;
    }

    public function getArgumentList(): array {
        return $this->argumentList;
    }

    /**
     * @param string|null $botName
     *
     * @return void
     */
    private function assignBotName(?string $botName): void {
        $botName       = str_replace('@', '', $botName ?? '');
        $this->botName = $botName === ''
            ? null
            : $botName;
    }

    private function assignArgumentList(?string $arguments) {
        $lines = explode("\n", $arguments ?? '');
        $list  = [];

        foreach ($lines as $line) {
            preg_match_all('/"(?:\\\\.|[^\\\\"])*"|\S+/', $line, $matches);
            foreach ($matches[0] ?? [] as $match) {
                $list[] = $match;
            }

            $list[] = "\n";
        }

        unset($list[count($list) - 1]);

        $list = array_map(function ($word) {
            if ($word === '"') {
                return $word;
            }

            return trim($word, '"');
        }, $list);

        $this->argumentList = array_filter($list ?? [], static fn($v) => ($v !== null && $v !== ''));
    }
}
