<?php

namespace CheeCodes\TelegramSdk;

use Illuminate\Support\Facades\Facade;

/**
 * @see \CheeCodes\TelegramSdk\Skeleton\SkeletonClass
 */
class TelegramSdkFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'telegram-sdk';
    }
}
