<?php

namespace CheeCodes\TelegramSdk;

use CheeCodes\TelegramSdk\Auth\WebhookGuard;
use CheeCodes\TelegramSdk\Console\Commands\RegisterWebhookCommand;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class TelegramSdkServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot() {

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path('telegram-sdk.php'),
            ], 'config');

            // Registering package commands.
            $this->commands([
                RegisterWebhookCommand::class,
            ]);
        }

        Auth::extend('telegram-webhook', function ($app, $name, array $config) {
            return new WebhookGuard(
                Auth::createUserProvider($config['provider']),
                request(),
                $config['storage_key'] ?? 'telegram_id'
            );
        });
    }

    /**
     * Register the application services.
     */
    public function register() {
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'telegram-sdk');

        $this->app->singleton('telegram-sdk', TelegramSdk::class);
    }
}
