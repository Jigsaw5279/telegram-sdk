<?php

namespace CheeCodes\TelegramSdk\Telegram;

use Illuminate\Support\Collection;

class InlineKeyboardMarkup extends Model
{
    /** @var array<array<\CheeCodes\TelegramSdk\Telegram\InlineKeyboardButton>> */
    protected array $inline_keyboard = [];

    private int   $_row            = 0;

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\InlineKeyboardButton[][]
     */
    public function getInlineKeyboard(): array {
        return $this->inline_keyboard;
    }

    /**
     * @param \CheeCodes\TelegramSdk\Telegram\InlineKeyboardButton[][] $inline_keyboard
     *
     * @return InlineKeyboardMarkup
     */
    public function setInlineKeyboard(array $inline_keyboard): InlineKeyboardMarkup {
        $this->inline_keyboard = $inline_keyboard;

        $this->_row = count($this->inline_keyboard);

        return $this;
    }

    public function nextRow(): InlineKeyboardMarkup {
        $this->_row++;

        $this->inline_keyboard[$this->_row] = [];

        return $this;
    }

    public function previousRow(): InlineKeyboardMarkup {
        $this->_row = max(0, $this->_row - 1);

        return $this;
    }

    public function addButton(array|Collection|InlineKeyboardButton $button): InlineKeyboardMarkup {
        $button = $this->setOrCreateModel($button, InlineKeyboardButton::class);

        $this->inline_keyboard[$this->_row][] = $button;

        return $this;
    }
}
