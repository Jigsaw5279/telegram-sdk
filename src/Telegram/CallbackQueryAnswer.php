<?php

namespace CheeCodes\TelegramSdk\Telegram;

class CallbackQueryAnswer extends Model
{
    protected string $callback_query_id;

    protected ?string $text;

    protected ?bool $show_alert;

    protected ?string $url;

    protected ?int $cache_time;

    /**
     * @return string
     */
    public function getCallbackQueryId(): string {
        return $this->callback_query_id;
    }

    /**
     * @param string $callback_query_id
     *
     * @return CallbackQueryAnswer
     */
    public function setCallbackQueryId(string $callback_query_id): CallbackQueryAnswer {
        $this->callback_query_id = $callback_query_id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string {
        return $this->text;
    }

    /**
     * @param string|null $text
     *
     * @return CallbackQueryAnswer
     */
    public function setText(?string $text): CallbackQueryAnswer {
        $this->text = $text;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getShowAlert(): ?bool {
        return $this->show_alert;
    }

    /**
     * @param bool|null $show_alert
     *
     * @return CallbackQueryAnswer
     */
    public function setShowAlert(?bool $show_alert): CallbackQueryAnswer {
        $this->show_alert = $show_alert;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string {
        return $this->url;
    }

    /**
     * @param string|null $url
     *
     * @return CallbackQueryAnswer
     */
    public function setUrl(?string $url): CallbackQueryAnswer {
        $this->url = $url;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCacheTime(): ?int {
        return $this->cache_time;
    }

    /**
     * @param int|null $cache_time
     *
     * @return CallbackQueryAnswer
     */
    public function setCacheTime(?int $cache_time): CallbackQueryAnswer {
        $this->cache_time = $cache_time;

        return $this;
    }


}
