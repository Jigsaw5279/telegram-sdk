<?php

namespace CheeCodes\TelegramSdk\Telegram\Types;

enum Chat: string
{
    case Private = 'private';
    case Group = 'group';
    case Supergroup = 'supergroup';
    case Channel = 'channel';
}
