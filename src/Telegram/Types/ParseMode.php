<?php

namespace CheeCodes\TelegramSdk\Telegram\Types;

enum ParseMode: string
{
    case Markdown = 'Markdown';
    case MarkdownV2 = 'MarkdownV2';
    case HTML = 'HTML';
}
