<?php

namespace CheeCodes\TelegramSdk\Telegram;

use Illuminate\Support\Collection;

class CallbackQuery extends Model
{
    protected string   $id;

    protected User     $from;

    protected ?Message $message;

    protected ?string  $inline_message_id;

    protected string   $chat_instance;

    protected ?string  $data;

    protected ?string  $game_short_name;

    /**
     * @return string
     */
    public function getId(): string {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return CallbackQuery
     */
    public function setId(string $id): CallbackQuery {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\User
     */
    public function getFrom(): User {
        return $this->from;
    }

    /**
     * @param \CheeCodes\TelegramSdk\Telegram\User $from
     *
     * @return CallbackQuery
     */
    public function setFrom(array|Collection|User $from): CallbackQuery {
        $this->from = $this->setOrCreateModel($from, User::class);

        return $this;
    }

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\Message|null
     */
    public function getMessage(): ?Message {
        return $this->message;
    }

    /**
     * @param \CheeCodes\TelegramSdk\Telegram\Message|null $message
     *
     * @return CallbackQuery
     */
    public function setMessage(array|Collection|Message|null $message): CallbackQuery {
        $this->message = $this->setOrCreateModel($message, Message::class);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInlineMessageId(): ?string {
        return $this->inline_message_id;
    }

    /**
     * @param string|null $inline_message_id
     *
     * @return CallbackQuery
     */
    public function setInlineMessageId(?string $inline_message_id): CallbackQuery {
        $this->inline_message_id = $inline_message_id;

        return $this;
    }

    /**
     * @return string
     */
    public function getChatInstance(): string {
        return $this->chat_instance;
    }

    /**
     * @param string $chat_instance
     *
     * @return CallbackQuery
     */
    public function setChatInstance(string $chat_instance): CallbackQuery {
        $this->chat_instance = $chat_instance;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getData(): ?string {
        return $this->data;
    }

    /**
     * @param string|null $data
     *
     * @return CallbackQuery
     */
    public function setData(?string $data): CallbackQuery {
        $this->data = $data;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGameShortName(): ?string {
        return $this->game_short_name;
    }

    /**
     * @param string|null $game_short_name
     *
     * @return CallbackQuery
     */
    public function setGameShortName(?string $game_short_name): CallbackQuery {
        $this->game_short_name = $game_short_name;

        return $this;
    }

    public function answer(array|Collection|CallbackQueryAnswer $answer) {
        /** @var \CheeCodes\TelegramSdk\Telegram\CallbackQueryAnswer $answer */
        $answer = $this->setOrCreateModel($answer, CallbackQueryAnswer::class);

        $answer->setCallbackQueryId($this->getId());

        \TelegramSdk::answerCallbackQuery($answer);
    }

}
