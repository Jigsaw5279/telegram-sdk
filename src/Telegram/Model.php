<?php

namespace CheeCodes\TelegramSdk\Telegram;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use ReturnTypeWillChange;

/** @template T */
abstract class Model implements \JsonSerializable
{
    public static function create(array|Collection|null $data = null) {
        $instance = new static();

        if ( !$data) {
            return $instance;
        }

        foreach ($data as $key => $value) {
            $method = 'set' . Str::studly($key);
            if (method_exists($instance, $method)) {
                $instance->{$method}($value);
            } else {
                Log::debug(sprintf('Method "%s" does not exist in "%s"', $method, $instance::class));
            }
        }

        return $instance;
    }

    /**
     * @param   $data
     * @param T $class
     *
     * @return mixed
     */
    protected function setOrCreateModel($data, $class) {
        if (is_null($data)) {
            return null;
        }

        return $data instanceof $class
            ? $data
            : $class::create($data);
    }

    public function jsonSerialize(): array {
        return $this->toArray();
    }

    public function toArray(): array {
        return array_filter(
            get_object_vars($this),
            fn($value, $key) => $value !== null && !str_starts_with($key, '_'),
            ARRAY_FILTER_USE_BOTH
        );
    }
}
