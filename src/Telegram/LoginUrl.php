<?php

namespace CheeCodes\TelegramSdk\Telegram;

class LoginUrl extends Model
{
    protected string $url;

    protected ?string $forward_text;

    protected ?string $bot_username;

    protected ?bool $request_write_access;

    /**
     * @return string
     */
    public function getUrl(): string {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return LoginUrl
     */
    public function setUrl(string $url): LoginUrl {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getForwardText(): ?string {
        return $this->forward_text;
    }

    /**
     * @param string|null $forward_text
     *
     * @return LoginUrl
     */
    public function setForwardText(?string $forward_text): LoginUrl {
        $this->forward_text = $forward_text;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBotUsername(): ?string {
        return $this->bot_username;
    }

    /**
     * @param string|null $bot_username
     *
     * @return LoginUrl
     */
    public function setBotUsername(?string $bot_username): LoginUrl {
        $this->bot_username = $bot_username;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getRequestWriteAccess(): ?bool {
        return $this->request_write_access;
    }

    /**
     * @param bool|null $request_write_access
     *
     * @return LoginUrl
     */
    public function setRequestWriteAccess(?bool $request_write_access): LoginUrl {
        $this->request_write_access = $request_write_access;

        return $this;
    }
}
