<?php

namespace CheeCodes\TelegramSdk\Telegram;

class User extends Model
{
    protected int     $id;

    protected bool    $is_bot;

    protected string  $first_name;

    protected ?string $last_name     = null;

    protected ?string $username      = null;

    protected ?string $language_code = null;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return User
     */
    public function setId(int $id): User {
        $this->id = $id;

        return $this;
    }

    /**
     * @return bool
     */
    public function isIsBot(): bool {
        return $this->is_bot;
    }

    /**
     * @param bool $is_bot
     *
     * @return User
     */
    public function setIsBot(bool $is_bot): User {
        $this->is_bot = $is_bot;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     *
     * @return User
     */
    public function setFirstName(string $first_name): User {
        $this->first_name = $first_name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string {
        return $this->last_name;
    }

    /**
     * @param string|null $last_name
     *
     * @return User
     */
    public function setLastName(?string $last_name): User {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string {
        return $this->username;
    }

    /**
     * @param string|null $username
     *
     * @return User
     */
    public function setUsername(?string $username): User {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLanguageCode(): ?string {
        return $this->language_code;
    }

    /**
     * @param string|null $language_code
     *
     * @return User
     */
    public function setLanguageCode(?string $language_code): User {
        $this->language_code = $language_code;

        return $this;
    }

    public function getFullName(): string {
        return trim(sprintf('%s %s', $this->getFirstName(), $this->getLastName()));
    }
}
