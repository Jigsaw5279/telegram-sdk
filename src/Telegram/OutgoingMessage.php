<?php

namespace CheeCodes\TelegramSdk\Telegram;

use CheeCodes\TelegramSdk\Telegram\Types\ParseMode;

class OutgoingMessage extends Model
{
    protected int|string $chat_id;

    protected string     $text;

    protected ?ParseMode $parse_mode;

    /** @var array<\CheeCodes\TelegramSdk\Telegram\MessageEntity>|null */
    protected ?array                $entities;

    protected ?int                  $reply_to_message_id;

    protected ?bool                 $allow_sending_without_reply;

    protected ?bool                 $disable_web_page_preview;

    protected ?bool                 $disable_notification;

    protected ?bool                 $protect_content;

    protected ?InlineKeyboardMarkup $reply_markup;

    /**
     * @return int|string
     */
    public function getChatId(): int|string {
        return $this->chat_id;
    }

    /**
     * @param int|string $chat_id
     *
     * @return OutgoingMessage
     */
    public function setChatId(int|string $chat_id): OutgoingMessage {
        $this->chat_id = $chat_id;

        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string {
        return $this->text;
    }

    /**
     * @param string $text
     *
     * @return OutgoingMessage
     */
    public function setText(string $text): OutgoingMessage {
        $this->text = $text;

        return $this;
    }

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\Types\ParseMode|null
     */
    public function getParseMode(): ?ParseMode {
        return $this->parse_mode;
    }

    /**
     * @param \CheeCodes\TelegramSdk\Telegram\Types\ParseMode|null $parse_mode
     *
     * @return OutgoingMessage
     */
    public function setParseMode(string|ParseMode|null $parse_mode): OutgoingMessage {
        if (is_null($parse_mode)) {
            $this->parse_mode = null;
        } else {
            $this->parse_mode = $parse_mode instanceof ParseMode
                ? $parse_mode
                : ParseMode::from($parse_mode);
        }

        return $this;
    }

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\MessageEntity[]|null
     */
    public function getEntities(): ?array {
        return $this->entities;
    }

    /**
     * @param \CheeCodes\TelegramSdk\Telegram\MessageEntity[]|null $entities
     *
     * @return OutgoingMessage
     */
    public function setEntities(?array $entities): OutgoingMessage {
        $parsed = [];

        foreach ($entities as $entity) {
            $parsed[] = $this->setOrCreateModel($entity, MessageEntity::class);
        }

        $this->entities = $parsed;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getReplyToMessageId(): ?int {
        return $this->reply_to_message_id;
    }

    /**
     * @param int|null $reply_to_message_id
     *
     * @return OutgoingMessage
     */
    public function setReplyToMessageId(?int $reply_to_message_id): OutgoingMessage {
        $this->reply_to_message_id = $reply_to_message_id;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getAllowSendingWithoutReply(): ?bool {
        return $this->allow_sending_without_reply;
    }

    /**
     * @param bool|null $allow_sending_without_reply
     *
     * @return OutgoingMessage
     */
    public function setAllowSendingWithoutReply(?bool $allow_sending_without_reply): OutgoingMessage {
        $this->allow_sending_without_reply = $allow_sending_without_reply;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDisableWebPagePreview(): ?bool {
        return $this->disable_web_page_preview;
    }

    /**
     * @param bool|null $disable_web_page_preview
     *
     * @return OutgoingMessage
     */
    public function setDisableWebPagePreview(?bool $disable_web_page_preview): OutgoingMessage {
        $this->disable_web_page_preview = $disable_web_page_preview;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDisableNotification(): ?bool {
        return $this->disable_notification;
    }

    /**
     * @param bool|null $disable_notification
     *
     * @return OutgoingMessage
     */
    public function setDisableNotification(?bool $disable_notification): OutgoingMessage {
        $this->disable_notification = $disable_notification;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getProtectContent(): ?bool {
        return $this->protect_content;
    }

    /**
     * @param bool|null $protect_content
     *
     * @return OutgoingMessage
     */
    public function setProtectContent(?bool $protect_content): OutgoingMessage {
        $this->protect_content = $protect_content;

        return $this;
    }

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\InlineKeyboardMarkup|null
     */
    public function getReplyMarkup(): ?InlineKeyboardMarkup {
        return $this->reply_markup;
    }

    /**
     * @param \CheeCodes\TelegramSdk\Telegram\InlineKeyboardMarkup|null $reply_markup
     *
     * @return OutgoingMessage
     */
    public function setReplyMarkup(?InlineKeyboardMarkup $reply_markup): OutgoingMessage {
        $this->reply_markup = $this->setOrCreateModel($reply_markup, InlineKeyboardMarkup::class);

        return $this;
    }

}
