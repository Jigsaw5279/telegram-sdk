<?php

namespace CheeCodes\TelegramSdk\Telegram;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class Message extends Model
{
    protected int      $message_id;

    protected ?string  $text = null;

    protected ?User    $from = null;

    protected Chat     $chat;

    protected ?User    $forward_from = null;

    protected ?Chat    $forward_from_chat = null;

    protected ?int     $forward_from_message_id = null;

    protected ?string  $forward_signature = null;

    protected ?string  $forward_sender_name = null;

    protected ?Carbon  $forward_date = null;

    protected ?bool    $is_automatic_forward = false;

    protected ?Message $reply_to_message = null;

    protected ?User    $via_bot = null;

    protected ?Carbon  $edit_date = null;

    protected ?bool    $has_protected_content = null;

    protected ?string  $media_group_id = null;

    protected ?string  $author_signature = null;

    protected mixed $reply_markup;

    /**
     * @var array<\CheeCodes\TelegramSdk\Telegram\MessageEntity>|null
     */
    protected ?array  $entities;

    protected ?Carbon $date;

    /**
     * @return mixed
     */
    public function getReplyMarkup(): mixed {
        return $this->reply_markup;
    }

    /**
     * @param mixed $reply_markup
     *
     * @return Message
     */
    public function setReplyMarkup(mixed $reply_markup): Message {
        $this->reply_markup = $reply_markup;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getForwardSignature(): ?string {
        return $this->forward_signature;
    }

    /**
     * @param string|null $forward_signature
     *
     * @return Message
     */
    public function setForwardSignature(?string $forward_signature): Message {
        $this->forward_signature = $forward_signature;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getForwardSenderName(): ?string {
        return $this->forward_sender_name;
    }

    /**
     * @param string|null $forward_sender_name
     *
     * @return Message
     */
    public function setForwardSenderName(?string $forward_sender_name): Message {
        $this->forward_sender_name = $forward_sender_name;

        return $this;
    }

    /**
     * @return \Illuminate\Support\Carbon|null
     */
    public function getForwardDate(): ?Carbon {
        return $this->forward_date;
    }

    /**
     * @param \Illuminate\Support\Carbon|null $forward_date
     *
     * @return Message
     */
    public function setForwardDate(int|Carbon|null $forward_date): Message {
        if ($forward_date instanceof Carbon || is_null($forward_date)) {
            $this->forward_date = $forward_date;
        } else {
            $this->forward_date = Carbon::createFromTimestampUTC($forward_date);
        }

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsAutomaticForward(): ?bool {
        return $this->is_automatic_forward;
    }

    /**
     * @param bool|null $is_automatic_forward
     *
     * @return Message
     */
    public function setIsAutomaticForward(?bool $is_automatic_forward): Message {
        $this->is_automatic_forward = $is_automatic_forward;

        return $this;
    }

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\Message|null
     */
    public function getReplyToMessage(): ?Message {
        return $this->reply_to_message;
    }

    /**
     * @param \CheeCodes\TelegramSdk\Telegram\Message|null $reply_to_message
     *
     * @return Message
     */
    public function setReplyToMessage(array|Collection|Message|null $reply_to_message): Message {
        $this->reply_to_message = $this->setOrCreateModel($reply_to_message, Message::class);

        return $this;
    }

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\User|null
     */
    public function getViaBot(): ?User {
        return $this->via_bot;
    }

    /**
     * @param \CheeCodes\TelegramSdk\Telegram\User|null $via_bot
     *
     * @return Message
     */
    public function setViaBot(array|Collection|User|null $via_bot): Message {
        $this->via_bot = $this->setOrCreateModel($via_bot, User::class);

        return $this;
    }

    /**
     * @return \Illuminate\Support\Carbon|null
     */
    public function getEditDate(): ?Carbon {
        return $this->edit_date;
    }

    /**
     * @param int|\Illuminate\Support\Carbon|null $edit_date
     *
     * @return Message
     */
    public function setEditDate(int|Carbon|null $edit_date): Message {
        if ($edit_date instanceof Carbon || is_null($edit_date)) {
            $this->edit_date = $edit_date;
        } else {
            $this->edit_date = Carbon::createFromTimestampUTC($edit_date);
        }

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getHasProtectedContent(): ?bool {
        return $this->has_protected_content;
    }

    /**
     * @param bool|null $has_protected_content
     *
     * @return Message
     */
    public function setHasProtectedContent(?bool $has_protected_content): Message {
        $this->has_protected_content = $has_protected_content;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMediaGroupId(): ?string {
        return $this->media_group_id;
    }

    /**
     * @param string|null $media_group_id
     *
     * @return Message
     */
    public function setMediaGroupId(?string $media_group_id): Message {
        $this->media_group_id = $media_group_id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthorSignature(): ?string {
        return $this->author_signature;
    }

    /**
     * @param string|null $author_signature
     *
     * @return Message
     */
    public function setAuthorSignature(?string $author_signature): Message {
        $this->author_signature = $author_signature;

        return $this;
    }

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\Chat|null
     */
    public function getForwardFromChat(): ?Chat {
        return $this->forward_from_chat;
    }

    /**
     * @param array|\Illuminate\Support\Collection|\CheeCodes\TelegramSdk\Telegram\Chat|null $forward_from_chat
     *
     * @return Message
     */
    public function setForwardFromChat(array|Collection|Chat|null $forward_from_chat): Message {
        $this->forward_from_chat = $this->setOrCreateModel($forward_from_chat, Chat::class);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getForwardFromMessageId(): ?int {
        return $this->forward_from_message_id;
    }

    /**
     * @param int|null $forward_from_message_id
     *
     * @return Message
     */
    public function setForwardFromMessageId(?int $forward_from_message_id): Message {
        $this->forward_from_message_id = $forward_from_message_id;

        return $this;
    }

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\User|null
     */
    public function getForwardFrom(): ?User {
        return $this->forward_from;
    }

    /**
     * @param \CheeCodes\TelegramSdk\Telegram\User|null $forward_from
     *
     * @return Message
     */
    public function setForwardFrom(array|Collection|User|null $forward_from): Message {
        $this->forward_from = $this->setOrCreateModel($forward_from, User::class);

        return $this;
    }

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\MessageEntity[]|null
     */
    public function getEntities(): ?array {
        return $this->entities;
    }

    /**
     * @param \CheeCodes\TelegramSdk\Telegram\MessageEntity[]|null $entities
     *
     * @return Message
     */
    public function setEntities(?array $entities): Message {
        if (is_null($entities)) {
            $this->entities = null;

            return $this;
        }

        $converted = [];
        foreach ($entities as $entity) {
            $converted[] = $this->setOrCreateModel($entity, MessageEntity::class);
        }

        $this->entities = $converted;

        return $this;
    }

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\User|null
     */
    public function getFrom(): ?User {
        return $this->from;
    }

    /**
     * @param \CheeCodes\TelegramSdk\Telegram\User|null $user
     *
     * @return Message
     */
    public function setFrom(array|Collection|User|null $user): Message {
        $this->from = $this->setOrCreateModel($user, User::class);

        return $this;
    }

    public function getDate(): ?Carbon {
        return $this->date;
    }

    public function setDate(int|Carbon $date): Message {
        if ($date instanceof Carbon) {
            $this->date = $date;
        } else {
            $this->date = Carbon::createFromTimestampUTC($date);
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string {
        return $this->text;
    }

    /**
     * @param string|null $text
     *
     * @return Message
     */
    public function setText(?string $text): Message {
        $this->text = $text;

        return $this;
    }

    public function replyWithMessage(string|array|Collection|OutgoingMessage $message) {
        $chatId  = $this->getChat()->getId();
        $replyTo = $this->getMessageId();

        if (is_string($message)) {
            $message = ['text' => $message];
        }

        /** @var \CheeCodes\TelegramSdk\Telegram\OutgoingMessage $message */
        $message = $this->setOrCreateModel($message, OutgoingMessage::class);

        $message->setReplyToMessageId($replyTo);
        $message->setChatId($chatId);
        $message->setAllowSendingWithoutReply(true);

        \TelegramSdk::sendMessage($message);
    }

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\Chat
     */
    public function getChat(): Chat {
        return $this->chat;
    }

    /**
     * @param array|\Illuminate\Support\Collection|\CheeCodes\TelegramSdk\Telegram\Chat $chat
     *
     * @return Message
     */
    public function setChat(array|Collection|Chat $chat): Message {
        $this->chat = $this->setOrCreateModel($chat, Chat::class);

        return $this;
    }

    public function getMessageId(): int {
        return $this->message_id;
    }

    /**
     * @param int $message_id
     *
     * @return Message
     */
    public function setMessageId(int $message_id): Message {
        $this->message_id = $message_id;

        return $this;
    }

    public function isReply():bool {
        return $this->reply_to_message !== null;
    }
}
