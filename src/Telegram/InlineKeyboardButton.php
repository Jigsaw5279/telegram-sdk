<?php

namespace CheeCodes\TelegramSdk\Telegram;

use Illuminate\Support\Collection;

class InlineKeyboardButton extends Model
{
    protected string    $text;

    protected ?string   $url;

    protected ?LoginUrl $login_url;

    protected ?string   $callback_data;

    protected ?string   $switch_inline_query;

    protected ?string   $switch_inline_query_current_chat;

    /**
     * @return string
     */
    public function getText(): string {
        return $this->text;
    }

    /**
     * @param string $text
     *
     * @return InlineKeyboardButton
     */
    public function setText(string $text): InlineKeyboardButton {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string {
        return $this->url;
    }

    /**
     * @param string|null $url
     *
     * @return InlineKeyboardButton
     */
    public function setUrl(?string $url): InlineKeyboardButton {
        $this->url = $url;

        return $this;
    }

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\LoginUrl|null
     */
    public function getLoginUrl(): ?LoginUrl {
        return $this->login_url;
    }

    /**
     * @param array|\Illuminate\Support\Collection|\CheeCodes\TelegramSdk\Telegram\LoginUrl|null $login_url
     *
     * @return InlineKeyboardButton
     */
    public function setLoginUrl(array|Collection|LoginUrl|null $login_url): InlineKeyboardButton {
        $this->login_url = $this->setOrCreateModel($login_url, LoginUrl::class);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCallbackData(): ?string {
        return $this->callback_data;
    }

    /**
     * @param string|null $callback_data
     *
     * @return InlineKeyboardButton
     */
    public function setCallbackData(?string $callback_data): InlineKeyboardButton {
        $this->callback_data = $callback_data;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSwitchInlineQuery(): ?string {
        return $this->switch_inline_query;
    }

    /**
     * @param string|null $switch_inline_query
     *
     * @return InlineKeyboardButton
     */
    public function setSwitchInlineQuery(?string $switch_inline_query): InlineKeyboardButton {
        $this->switch_inline_query = $switch_inline_query;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSwitchInlineQueryCurrentChat(): ?string {
        return $this->switch_inline_query_current_chat;
    }

    /**
     * @param string|null $switch_inline_query_current_chat
     *
     * @return InlineKeyboardButton
     */
    public function setSwitchInlineQueryCurrentChat(?string $switch_inline_query_current_chat): InlineKeyboardButton {
        $this->switch_inline_query_current_chat = $switch_inline_query_current_chat;

        return $this;
    }

}
