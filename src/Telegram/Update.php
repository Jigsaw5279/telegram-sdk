<?php

namespace CheeCodes\TelegramSdk\Telegram;

use Illuminate\Support\Collection;

class Update extends Model
{
    protected ?int           $update_id      = null;

    protected ?Message       $message        = null;

    protected ?CallbackQuery $callback_query = null;

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\CallbackQuery|null
     */
    public function getCallbackQuery(): ?CallbackQuery {
        return $this->callback_query;
    }

    /**
     * @param \CheeCodes\TelegramSdk\Telegram\CallbackQuery|null $callback_query
     *
     * @return Update
     */
    public function setCallbackQuery(array|Collection|CallbackQuery|null $callback_query): Update {
        $this->callback_query = $this->setOrCreateModel($callback_query, CallbackQuery::class);

        return $this;
    }

    public function isValid(): bool {
        return $this->getUpdateId() !== null;
    }

    /**
     * @return int
     */
    public function getUpdateId(): ?int {
        return $this->update_id;
    }

    /**
     * @param int $update_id
     */
    public function setUpdateId(int $update_id): void {
        $this->update_id = $update_id;
    }

    /**
     * @return \CheeCodes\TelegramSdk\Telegram\Message|null
     */
    public function getMessage(): ?Message {
        return $this->message;
    }

    public function setMessage(array|Collection|Message $message): Update {
        $this->message = $this->setOrCreateModel($message, Message::class);

        return $this;
    }

    public function isCallbackQuery() {
        return $this->callback_query !== null;
    }

}
