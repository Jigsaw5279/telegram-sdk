<?php

namespace CheeCodes\TelegramSdk\Telegram;

use CheeCodes\TelegramSdk\Telegram\Types\Chat as ChatType;

class Chat extends Model
{
    protected int      $id;

    protected ChatType $type;

    protected ?string  $username;

    protected ?string  $first_name;

    protected ?string  $last_name;

    protected ?string  $title;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Chat
     */
    public function setId(int $id): Chat {
        $this->id = $id;

        return $this;
    }

    /**
     * @return ChatType
     */
    public function getType(): Types\Chat {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Chat
     */
    public function setType(string|ChatType $type): Chat {
        $this->type = $type instanceof ChatType
            ? $type
            : ChatType::from($type);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string {
        return $this->username;
    }

    /**
     * @param string|null $username
     *
     * @return Chat
     */
    public function setUsername(?string $username): Chat {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string {
        return $this->first_name;
    }

    /**
     * @param string|null $first_name
     *
     * @return Chat
     */
    public function setFirstName(?string $first_name): Chat {
        $this->first_name = $first_name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string {
        return $this->last_name;
    }

    /**
     * @param string|null $last_name
     *
     * @return Chat
     */
    public function setLastName(?string $last_name): Chat {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void {
        $this->title = $title;
    }
}
