<?php

namespace CheeCodes\TelegramSdk\Auth;

use App\Models\User;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;
use TelegramSdk;

class WebhookGuard implements \Illuminate\Contracts\Auth\Guard
{
    use GuardHelpers;

    /**
     * @var \Illuminate\Http\Request
     */
    protected Request $request;

    /**
     * @var mixed|string
     */
    protected string $storageKey;

    public function __construct(UserProvider $provider, Request $request, string $storageKey = 'telegram_id') {
        $this->request    = $request;
        $this->provider   = $provider;
        $this->storageKey = $storageKey;
    }

    /**
     * @inheritDoc
     */
    public function user() {
        if ( !is_null($this->user)) {
            return $this->user;
        }

        $user = null;

        $update = TelegramSdk::getUpdate();

        if ($update !== null) {
            if ($update->isCallbackQuery()) {
                $from       = $update->getCallbackQuery()?->getFrom();
                $telegramId = $from->getId();
            } else {
                $from       = $update->getMessage()?->getFrom() ?? $update->getMessage()?->getForwardFrom();
                $telegramId = $from?->getId();
            }

            if ( !$telegramId || !$from) {
                return null;
            }

            $user = User::firstOrCreate([
                $this->storageKey => $telegramId,
            ], [
                $this->storageKey => $telegramId,
                'name'            => trim(sprintf('%s %s', $from->getFirstName(), $from->getLastName())),
                'username'        => $from->getUsername(),
                'is_bot'          => $from->isIsBot(),
            ]);
        }

        return $this->user = $user;
    }

    /**
     * @inheritDoc
     */
    public function validate(array $credentials = []) {
        if (empty($credentials['telegram_id'])) {
            return false;
        }

        $credentials = ['telegram_id' => $credentials['telegram_id']];

        if ($this->provider->retrieveByCredentials($credentials)) {
            return true;
        }

        return false;
    }
}
