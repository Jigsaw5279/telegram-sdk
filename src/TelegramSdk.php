<?php

namespace CheeCodes\TelegramSdk;

use CheeCodes\TelegramSdk\Commands\BotCommandBase;
use CheeCodes\TelegramSdk\Support\CommandParser;
use CheeCodes\TelegramSdk\Telegram\CallbackQueryAnswer;
use CheeCodes\TelegramSdk\Telegram\OutgoingMessage;
use CheeCodes\TelegramSdk\Telegram\Update;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class TelegramSdk
{
    private static ?Update $parsedUpdate = null;

    public function __construct(protected CommandParser $commandParser) {
    }

    public function handleWebhook(): bool {
        $update = $this->getUpdate();

        if ( !$update) {
            return false;
        }

        if ($update->isCallbackQuery()) {
            $commandString = $update->getCallbackQuery()?->getData() ?? '';
        } else {
            $commandString = $update->getMessage()?->getText() ?? '';
        }

        $this->commandParser->parse($commandString);

        $this->invokeCommand($update);

        return true;
    }

    protected function invokeCommand(Update $update) {
        $commandName = $this->commandParser->getCommandName();

        $fqdn = $this->findCommandFqdn($commandName);

        if ( !$fqdn) {
            return;
        }

        $instance = app()->make($fqdn);

        if ($instance instanceof BotCommandBase) {
            $instance->handle($update, $this->commandParser->getArgumentList(), $commandName,
                $this->commandParser->getBotName());
        }
    }

    private function createUrl(string $method): string {
        $secret = config('services.telegram.token');

        return sprintf('https://api.telegram.org/bot%s/%s', $secret, $method);
    }

    public function sendMessage(OutgoingMessage $message) {
        $this->raw(__FUNCTION__, $message->toArray());
    }

    public function answerCallbackQuery(CallbackQueryAnswer $answer) {
        $this->raw(__FUNCTION__, $answer->toArray());
    }

    public function raw(string $method, array $data): Response {
        return Http::post($this->createUrl($method), $data);
    }

    /**
     * @param string|null $commandName
     *
     * @return mixed|string
     */
    protected function findCommandFqdn(?string $commandName): ?string {
        $studly = Str::studly($commandName);

        $fqdn = 'telegram.' . $commandName;

        if (app()->has($fqdn)) {
            \Log::info('Has FQDN', compact('fqdn'));

            return $fqdn;
        }

        foreach (config('telegram-sdk.command_namespaces', []) as $namespace) {
            $candidate = sprintf('%s\%sCommand', $namespace, $studly);
            \Log::info('Looking in namespace', compact('namespace', 'candidate'));
            if (class_exists($candidate)) {
                \Log::info('Candidate matches');

                return $candidate;
            }
        }

        \Log::error('No candidate found');

        return null;
    }

    public function getUpdate(?Request $request = null) {
        if (self::$parsedUpdate) {
            return self::$parsedUpdate;
        }

        $data   = $request?->all() ?? request()->all();
        $update = Update::create($data);

        if ($update->isValid()) {
            static::$parsedUpdate = $update;
        }

        return self::$parsedUpdate;
    }
}
