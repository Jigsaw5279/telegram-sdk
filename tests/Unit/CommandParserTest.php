<?php

namespace CheeCodes\TelegramSdk\Tests\Unit;

use CheeCodes\TelegramSdk\Support\CommandParser;
use PHPUnit\Framework\TestCase;

class CommandParserTest extends TestCase
{
    private $commandParser;

    protected function setUp(): void {
        parent::setUp();
        $this->commandParser = new CommandParser();
    }

    /**
     * @test
     * @dataProvider commandStringProvider
     */
    public function it_parses_the_correct_command_from_a_string(string $commandString, ?string $expectedCommand) {
        $this->commandParser->parse($commandString);

        $this->assertSame($expectedCommand, $this->commandParser->getCommandName());
    }

    /**
     * @test
     * @dataProvider commandStringProvider
     */
    public function it_parses_the_bot_name(string $commandString, ?string $_, ?string $expectedBotName) {
        $this->commandParser->parse($commandString);

        $this->assertSame($expectedBotName, $this->commandParser->getBotName());
    }

    /**
     * @test
     * @dataProvider commandStringProvider
     */
    public function it_parses_the_argument_list(
        string $commandString,
        ?string $_,
        ?string $__,
        ?array $expectedArgumentList
    ) {
        $this->commandParser->parse($commandString);

        $this->assertSame($expectedArgumentList, $this->commandParser->getArgumentList());
    }

    /**
     * @return array<array>
     */
    public function commandStringProvider(): array {
        return [
            "/command"                                                                            => [
                "/command",
                "command",
                null,
                [],
            ],
            "/myCommand"                                                                          => [
                "/myCommand",
                "myCommand",
                null,
                [],
            ],
            "text before /myCommand"                                                              => [
                "text before /myCommand",
                "myCommand",
                null,
                [],
            ],
            "/myCommand text after"                                                               => [
                "/myCommand text after",
                "myCommand",
                null,
                ['text', 'after'],
            ],
            "text before /myCommand text after"                                                   => [
                "text before /myCommand text after",
                "myCommand",
                null,
                ['text', 'after'],
            ],
            "/myCommand@mybot"                                                                    => [
                "/myCommand@mybot",
                "myCommand",
                "mybot",
                [],
            ],
            "text before /myCommand@mybot"                                                        => [
                "text before /myCommand@mybot",
                "myCommand",
                "mybot",
                [],
            ],
            "/myCommand@mybot text after"                                                         => [
                "/myCommand@mybot text after",
                "myCommand",
                "mybot",
                ['text', 'after'],
            ],
            "text before /myCommand@mybot text after"                                             => [
                "text before /myCommand@mybot text after",
                "myCommand",
                "mybot",
                ['text', 'after'],
            ],
            "no command in sight,  just people enjoying the moment"                               => [
                "no command in sight,  just people enjoying the moment",
                null,
                null,
                [],
            ],
            '/myCommand@mybot mein "text after" command'                                          => [
                '/myCommand@mybot mein "text after" command',
                "myCommand",
                "mybot",
                ['mein', 'text after', 'command'],
            ],
            '/myCommand@mybot "text after" command'                                               => [
                '/myCommand@mybot "text after" command',
                "myCommand",
                "mybot",
                ['text after', 'command'],
            ],
            '/myCommand@mybot "text after"'                                                       => [
                '/myCommand@mybot "text after"',
                "myCommand",
                "mybot",
                ['text after'],
            ],
            '/myCommand@mybot ein text " mit double quotes'                                       => [
                '/myCommand@mybot ein text " mit double quotes',
                "myCommand",
                "mybot",
                ['ein', 'text', '"', 'mit', 'double', 'quotes'],
            ],
            '/myCommand@mybot mein "text after" command "with a second" pair'                     => [
                '/myCommand@mybot mein "text after" command "with a second" pair',
                "myCommand",
                "mybot",
                ['mein', 'text after', 'command', 'with a second', 'pair'],
            ],
            "/myCommand@mybot mein \"text after\" command with a line break"                      => [
                "/myCommand@mybot mein \"text after\" command with\na line break",
                "myCommand",
                "mybot",
                ['mein', 'text after', 'command', 'with', "\n", 'a', 'line', 'break'],
            ],
            '/myCommand@mybot mein "text after" command "with a second" pair " and double quotes' => [
                '/myCommand@mybot mein "text after" command "with a second" pair " and double quotes',
                "myCommand",
                "mybot",
                ['mein', 'text after', 'command', 'with a second', 'pair', '"', 'and', 'double', 'quotes'],
            ],
            '/command 0'                                                                          => [
                '/command 0',
                'command',
                null,
                ['0'],
            ],
            '/command 1'                                                                          => [
                '/command 1',
                'command',
                null,
                ['1'],
            ],
        ];
    }
}
