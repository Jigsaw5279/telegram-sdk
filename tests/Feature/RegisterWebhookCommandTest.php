<?php

namespace CheeCodes\TelegramSdk\Tests\Feature;

use CheeCodes\TelegramSdk\Tests\TestCase;
use Illuminate\Http\Client\Request;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class RegisterWebhookCommandTest extends TestCase
{
    /** @test */
    public function when_the_route_does_not_exist_an_error_is_shown() {
        $this->artisan('telegram:webhook my-route')
             ->expectsOutput('Route "my-route" does not exist')
             ->assertFailed();
    }

    /** @test */
    public function the_set_webhook_uri_is_called() {
        Route::get('/telegram/webhook', fn() => 'ok')
             ->name('telegram-sdk.webhook');

        $route  = URL::route('telegram-sdk.webhook');
        $secret = config('services.telegram.token');

        Http::fake([
            '*' => Http::response(['ok' => true, 'result' => true, 'description' => 'Webhook was set']),
        ]);

        $this->artisan('telegram:webhook')
             ->expectsOutput(sprintf('Webhook was set to "%s"', $route))
             ->assertSuccessful();

        Http::assertSent(function (Request $request) use ($route, $secret) {
            return $request['url'] === $route
                   && $request->method() === 'POST'
                   && $request->url() === 'https://api.telegram.org/bot' . $secret . '/setWebhook';
        });
    }
}
