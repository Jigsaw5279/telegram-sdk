<?php

namespace CheeCodes\TelegramSdk\Tests;

use CheeCodes\TelegramSdk\TelegramSdkServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function getPackageProviders($app) {
        return [
            TelegramSdkServiceProvider::class,
        ];
    }
}
